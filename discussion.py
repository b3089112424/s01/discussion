# commands used before code along
# setting up virtual environment
	# python -m venv graphenv

# activating the virtual environment
	# source graphenv/Scripts/activate
# deactivate the venv
	# deactivate

# upgrading pip
	# python.exe -m pip install --upgrade pip

# installing matplotlib and pandas
	# python -m pip install -U matplotlib
	# pip install pandas

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

# define the figure size
	# figure(figsize = (width, height))
		# width - default size 6.4 inches
		# height - default size 4.8 inches
plt.figure(figsize=(10,5))

x1 = [0, 1, 2, 3, 4]
y1 = [0, 2, 4, 6, 8]

# plt.plot() - draws the figure/graph
# x1, y1 - sets the values of points for x and y axis
# 'g.--' - (g) green; (--) dashes with (.) point marker
# label = 'greenline' - for ease of use of the line chart
# markersize = 20 - adjusting the markersize
# markeredgecolor = 'blue' - sets the marker edge (border) color
plt.plot(x1, y1, 'g.--', label = 'greenline', markersize=20, markeredgecolor='blue')

x2 = np.arange(0, 4.5, 0.5)
# arrange (start, stop, step)
# start - first value
# stop - end value; but it will not be included in the array
# step - 'spacing' between the values in the array
# value of x2 = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4]

plt.plot(x2, x2**2, 'r', label='redline')
# x2**2 - setting the values of y axis to numbers in x2 raised to 2
# 'r' - red solid

font = {'family': 'serif', 'color': 'darkred', 'weight': 'normal', 'size': 16}
plt.title('Marketing sale!', fontdict = font)

plt.xlabel('This is the x-axis')
plt.ylabel('This is the y-axis')

plt.xticks([0, 1, 2, 3, 4])
plt.xticks([0, 2, 4, 6, 8, 10, 12, 14, 16])

plt.savefig('linegraph.png', dpi=100)

plt.legend()
plt.show()

# [Section] Line Chart with CSV data
gas = pd.read_csv('./gas_prices.csv')

plt.figure(figsize=(8,5))
plt.title('Gas prices', fontdict={'family': 'monospace'})

plt.ylabel('US Dollars')
plt.xlabel('Year')

# plt.plot(gas.Year, gas.USA, 'b.-', label='United States')
	
# plt.plot(gas.Year, gas.Canada, 'rs--', label='Canada')
	
# plt.plot(gas.Year, gas['South Korea'], 'm*:', label='South Korea')

# plt.plot(gas.Year, gas.Australia, 'c+-', label='Australia')

# use a loop to plot the values
search_country = ['USA', 'Canada', 'South Korea', 'Australia']

for country in gas:
	# if country in search_country:
	# 	plt.plot(gas.Year, gas[country], marker='.')

	# every country will be printed in the graph
	if country != 'Year':
		plt.plot(gas.Year, gas[country], marker='*')

# the x axis will have an interval of 3
plt.xticks(gas.Year[::3])

plt.legend()
plt.show()

# [Section] Bar Graph
plt.figure(figsize = (6,4))

labels = ['Horror', 'Comedy', 'Action', 'Drama', 'Sci-Fi', 'Romance']
values = [18, 30, 38, 24, 12, 59]
bar_color = ['magenta', 'cyan', 'orange', 'pink', 'blue', 'limegreen']

plt.title('Top Movie Genre', fontdict = font)
plt.bar(labels, values, color = bar_color)
plt.show()

# [Section] Histogram
# define a path for csv file
fifa = pd.read_csv('./fifa_data.csv')

# bins
bins = [30, 40, 50, 60, 70, 80, 90, 100]

plt.title('Distribution of Player Skills in FIFA 2018')
plt.xlabel('NUmber of Players')
plt.ylabel('Skill Level')

plt.xticks(bins)
plt.hist(fifa.Overall, bins = bins, color = '#abcdef')

plt.show()

# [Section] Box and Whiskers
plt.figure(figsize=(5,8))

plt.title('Football Team Comparision')
plt.xlabel('FIFA Club')
plt.ylabel('FIFA Overall Rating')
labels = ['FC Barcelona', 'Real Madrid', 'NE Revolution']
barcelona = fifa.loc[fifa.Club == 'FC Barcelona']['Overall']
madrid = fifa.loc[fifa.Club == 'Real Madrid']['Overall']
revs = fifa.loc[fifa.Club == 'New England Revolution']['Overall']

# holds the linewidth property and its value
median_props = {'linewidth': 3}

# boxplot(x) - makes box and 
	# x - list of values
	# labels - label of each data set, the number of labels must be equal with the dimensions of x
	# patch_artist - allows us to change the fill color of the boxes
	# medianprops - responsible for the thickness of the box
boxes = plt.boxplot([barcelona, madrid, revs], labels = labels, patch_artist = True, medianprops = median_props)
for box in boxes['boxes']:
	box.set(color = '#abcdef', linewidth=4)
	box.set(facecolor = '#cecece')

plt.show()

# [Section] Pie Chart
left = fifa.loc[fifa['Preferred Foot']=='Left'].count()[0]
right = fifa.loc[fifa['Preferred Foot']=='Right'].count()[0]
print('Left', left)
print('Right', right)

labels = ['Left', 'Right']
colors = ['#9bbfe0', '#fbe29f']

plt.title('Foot Preferrence of FIFA Players')
plt.pie([left, right], labels = labels, colors = colors, autopct = '%.2f %%')

plt.show()



# [SECTION] Another Example of Pie Chart

	# a. Weight distribution of players
		# weight value by default would look like '159lbs'
		# 'lbs' shoud be removed and 159 be converted into integer for manipulation and setting condition
		# int() - is for converting the string to integer
		# strip() - removes the specified value of string
		# type() - checks the data type
fifa.Weight = [int(x.strip('lbs')) if type(x) == str else x for x in fifa.Weight]

	# b. Create 5 variables to filter our player's weight. 
		# Use the loc[] and count() functions to get all player's weight on a certain condition
light = fifa.loc[fifa.Weight < 125].count()[0]
light_medium = fifa.loc[(fifa.Weight >= 125) & (fifa.Weight < 150)].count()[0]
medium = fifa.loc[(fifa.Weight >= 150) & (fifa.Weight < 175)].count()[0]
medium_heavy = fifa.loc[(fifa.Weight >= 175) & (fifa.Weight < 200)].count()[0]
heavy = fifa[fifa.Weight >= 200].count()[0]
	
	# c. Setting the title
plt.title('Weight Distribution of FIFA Players (in lbs)')

	# d. Declaring variables for weights and labels for the pie chart
		# weight and labels should have the same size
weights = [light, light_medium, medium, medium_heavy, heavy]
labels = ['Under 125', '125-150', '150-175', '175-200', 'Over 200']

	# e. Create a tuple list called explode with ordered values from light to heavy
explode = (.4, .2, 0 , 0, .4) 

	# f. pie(x) - to create a pie chart
		# x - values
		# labels - for the parts of the pie
		# autopct='%.2f %%' - enables us to display the percentage value of each slice using Python string formatting
			# .2f % - means 2 decimal places with percent symbol
		# pctdistance = 0.8 - ratio between the center of each pie slice and the start of the text generated by autopct.
		# explode - offsetting of pie slice
plt.pie(weights, labels=labels, autopct='%.2f %%', pctdistance=0.8, explode=explode)

	# g. Show the created graph
plt.show()